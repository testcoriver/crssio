#' CRSSIO: Package to manage the input/output of CRSS data.
#'
#' The CRSSIO package provides functions to generate input files commmonly used by the 
#' Colorado River Simulation System (CRSS) from publicly available data, as well as 
#' functions to modify these files. Figures that are commonly used to QA/QC and 
#' undersand CRSS results are also automated with this package.
#' 
#' @section Major Input Related Functions:
#' \itemize{
#'  \item \code{\link{createCRSSDNFInputFiles}}
#'  \item \code{\link{changeStartDate}}
#' }
#' 
#' @section Major Output Related Functions:
#' \itemize{
#'  \item \code{\link{createSysCondTable}}
#' }
#'
#' @docType package
#' @name CRSSIO
NULL